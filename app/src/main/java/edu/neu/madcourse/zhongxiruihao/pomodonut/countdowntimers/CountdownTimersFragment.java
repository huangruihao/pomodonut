package edu.neu.madcourse.zhongxiruihao.pomodonut.countdowntimers;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.madcourse.zhongxiruihao.pomodonut.R;
import edu.neu.madcourse.zhongxiruihao.pomodonut.countdowntimers.models.Event;
import edu.neu.madcourse.zhongxiruihao.pomodonut.dayview.models.Action;
import edu.neu.madcourse.zhongxiruihao.pomodonut.editevent.EditEventActivity;
import edu.neu.madcourse.zhongxiruihao.pomodonut.globals.Globals;
import edu.neu.madcourse.zhongxiruihao.pomodonut.notifications.TimerNotification;
import edu.neu.madcourse.zhongxiruihao.pomodonut.utils.Utils;

/**
 * A placeholder fragment containing a simple view.
 */
public class CountdownTimersFragment extends Fragment {

    private static final String EVENTS_ARGS_KEY = "events args key";
    private static final String PAGE_ARGS_KEY = "page args key";

    private static final int MILLISECONDS_PER_SECOND = 1000;

    public static final String EVENT_NAME_BUNDLE_KEY = "event name bundle key";
    public static final String EVENT_TIME_BUNDLE_KEY = "event duration bundle key";

    private static final int UPDATE_TIME_LEFT_WHAT = 1;

    private static final String TIME_KEY = "time key";

    private static final String TIME_IS_UP = "00:00:00";

    private static final String TIME_LEFT = "Time Left: ";
    private static final String OVERTIME = "Overtime: ";

    private View root;
    private View[] timers;

    private TextView currentTimerText;
    private Action currentAction;

    private Timer mTimer;
    private Timer notificationTimer;

    private Event[] events;
    public int page;  // page index

    private boolean overtime = false;
    private int textColor;

    private Handler handler;

    TimerNotification timerNotification;

    private static int timerIds[] = {R.id.timer_0, R.id.timer_1, R.id.timer_2,
            R.id.timer_3, R.id.timer_4, R.id.timer_5};

    public CountdownTimersFragment() {}

    public static CountdownTimersFragment newInstance(Event[] events, int page) {
        CountdownTimersFragment fragment = new CountdownTimersFragment();
        Bundle args = new Bundle();
        args.putSerializable(EVENTS_ARGS_KEY,events);
        args.putInt(PAGE_ARGS_KEY, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            events = (Event[]) getArguments().getSerializable(EVENTS_ARGS_KEY);
            page = getArguments().getInt(PAGE_ARGS_KEY);
        }
        mTimer = new Timer();
        handler = new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message message) {
                switch (message.what) {
                    case UPDATE_TIME_LEFT_WHAT:
                        Bundle data = message.getData();
                        String time = data.getString(TIME_KEY);
                        if (time != null && time.equals(TIME_IS_UP)) {
                            overtime = true;
                            currentTimerText.setTextColor(Color.RED);
                        }
                        currentTimerText.setText(time);
                }
            }
        };
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_countdown_timers, container, false);
        if (events.length == 1) {
            ((LinearLayout) root.findViewById(R.id.outer_linear_layout))
                    .setGravity(Gravity.START);
        }

        timers = new View[CountdownTimersActivity.TIMERS_PER_PAGE];
        for (int i = 0; i < timers.length; ++i) {
            timers[i] = root.findViewById(timerIds[i]);
        }

        if (events != null) {
            int count = events.length;
            if (page == 0) {
                ((LinearLayout) timers[0]).removeAllViews();
                timers[0].setBackground(ContextCompat.getDrawable(getContext(),
                        R.drawable.new_countdown_timer_background));
                timers[0].setOnClickListener(new View.OnClickListener() {  // new event
                    @Override
                    public void onClick(View v) {
                        Bundle data = new Bundle();
                        data.putString(EVENT_NAME_BUNDLE_KEY, null);
                        data.putLong(EVENT_TIME_BUNDLE_KEY, -1);
                        Intent intent = new Intent(getActivity(), EditEventActivity.class);
                        intent.putExtras(data);
                        startActivity(intent);
                    }
                });
                for (int i = 1; i < timers.length; ++i) {
                    if (i >= count) {
                        timers[i].setVisibility(View.GONE);
                    } else {
                        ((TextView) timers[i].findViewById(R.id.text_timer_title))
                                .setText(Utils.shortenString(getContext(), events[i - 1].name));
                        currentTimerText = (TextView) timers[i].findViewById(R.id.text_timer_time);
                        textColor = currentTimerText.getCurrentTextColor();
                        currentTimerText.setText(Utils.formatMillisecond(events[i - 1].duration));
                        setTimerColor(timers[i], i - 1);
                        setRightButtonOnClickListener(timers[i], events[i - 1], i);
                        setLeftButtonOnclickListener(timers[i], events[i - 1], i);
                    }
                }
            } else {
                for (int i = 0; i < timers.length; ++i) {
                    if (i >= count) {
                        timers[i].setVisibility(View.GONE);
                    } else {
                        ((TextView) timers[i].findViewById(R.id.text_timer_title))
                                .setText(Utils.shortenString(getContext(), events[i].name));
                        currentTimerText = (TextView) timers[i].findViewById(R.id.text_timer_time);
                        textColor = currentTimerText.getCurrentTextColor();
                        currentTimerText.setText(Utils.formatMillisecond(events[i].duration));
                        setTimerColor(timers[i], i);
                        setRightButtonOnClickListener(timers[i], events[i], i);
                        setLeftButtonOnclickListener(timers[i], events[i], i);
                    }
                }
            }
        } else {  // disable all
            for (View timer: timers) {
                timer.setVisibility(View.GONE);
            }
        }

        return root;
    }

    private void setLeftButtonOnclickListener(final View timer, final Event event, final int index) {
        timer.findViewById(R.id.button_timer_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((Globals) getActivity().getApplication()).currentActiveTimerIndex == -1) {  // start
                    startTimer(timer, index, event.name, event.duration);
                    currentAction = new Action();
                    currentAction.eventName = event.name;
                    currentAction.startTime = System.currentTimeMillis();
                    event.happens();
                    event.save();
                } else if (((Globals) getActivity().getApplication()).currentActiveTimerIndex
                        == CountdownTimersActivity.TIMERS_PER_PAGE * page + index) {
                    if (((Globals) getActivity().getApplication()).hasTimerCountingDown) {  // pause
                        pauseTimer(timer);
                    } else {  // resume
                        startTimer(timer, index, event.name, event.duration);
                    }
                } else {  // invalid
                    Toast.makeText(getActivity(), getString(R.string.already_timer_counting_down),
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void setRightButtonOnClickListener(final View timer, final Event event, final int index) {
        timer.findViewById(R.id.button_timer_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((Globals) getActivity().getApplication()).currentActiveTimerIndex
                        == CountdownTimersActivity.TIMERS_PER_PAGE * page + index) {  // stop
                    stopTimer(timer, event.name, event.duration);
                } else {  // edit
                    Bundle data = new Bundle();
                    data.putString(EVENT_NAME_BUNDLE_KEY, event.name);
                    data.putLong(EVENT_TIME_BUNDLE_KEY, event.duration);
                    Intent intent = new Intent(getActivity(), EditEventActivity.class);
                    intent.putExtras(data);
                    startActivity(intent);
                }
            }
        });
    }

    private void setTimerColor(View timer, int index) {
        ArrayList<Integer> colors = Utils.getColors();
        LayerDrawable timerBackground = (LayerDrawable) timer.getBackground();
        GradientDrawable circle = (GradientDrawable) timerBackground.findDrawableByLayerId(R.id.background_circle);
        circle.setStroke(Utils.dpToPx(getContext(), 2),
                colors.get(page * CountdownTimersActivity.TIMERS_PER_PAGE + index));
        GradientDrawable line = (GradientDrawable) timerBackground.findDrawableByLayerId(R.id.background_line);
        line.setStroke(Utils.dpToPx(getContext(), 1),
                colors.get(page * CountdownTimersActivity.TIMERS_PER_PAGE + index));
    }

    private void startTimer(final View timer, int index, final String eventName, long eventTime) {
        ((Globals) getActivity().getApplication()).hasTimerCountingDown = true;
        ((Globals) getActivity().getApplication()).currentActiveTimerIndex
                = page * CountdownTimersActivity.TIMERS_PER_PAGE + index;
        if (getResources().getDisplayMetrics().densityDpi == DisplayMetrics.DENSITY_HIGH) {
            ((ImageButton) timer.findViewById(R.id.button_timer_left))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_pause_black_24dp));
            ((ImageButton) timer.findViewById(R.id.button_timer_right))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_stop_black_24dp));
        } else {
            ((ImageButton) timer.findViewById(R.id.button_timer_left))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_pause_black_36dp));
            ((ImageButton) timer.findViewById(R.id.button_timer_right))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_stop_black_36dp));
        }
        currentTimerText = (TextView) timer.findViewById(R.id.text_timer_time);
        mTimer.cancel();
        mTimer.purge();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                long timeLeft = Utils.parseMillisecond(currentTimerText.getText().toString());
                if (overtime) {
                    timeLeft += MILLISECONDS_PER_SECOND;
                } else {
                    timeLeft -= MILLISECONDS_PER_SECOND;
                }
                Message message = new Message();
                message.what = UPDATE_TIME_LEFT_WHAT;
                Bundle data = new Bundle();
                data.putString(TIME_KEY, Utils.formatMillisecond(timeLeft));
                message.setData(data);
                handler.sendMessage(message);
            }
        }, 0, MILLISECONDS_PER_SECOND);

        timerNotification = new TimerNotification((NotificationManager)
                getActivity().getSystemService(Context.NOTIFICATION_SERVICE));
        Intent intent = new Intent(getActivity(), CountdownTimersActivity.class);
        timerNotification.makeNotification(getActivity(), intent, eventName, eventName);
        notificationTimer = new Timer();
        notificationTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (overtime) {
                    try {
                        timerNotification.updateNotification(eventName,
                                OVERTIME + currentTimerText.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        timerNotification.updateNotification(eventName,
                                TIME_LEFT + currentTimerText.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 0, MILLISECONDS_PER_SECOND);
    }

    private void pauseTimer(View timer) {
        mTimer.cancel();
        if (getResources().getDisplayMetrics().densityDpi == DisplayMetrics.DENSITY_HIGH) {
            ((ImageButton) timer.findViewById(R.id.button_timer_left))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_play_arrow_black_24dp));
        } else {
            ((ImageButton) timer.findViewById(R.id.button_timer_left))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_play_arrow_black_36dp));
        }
        ((Globals) getActivity().getApplication()).hasTimerCountingDown = false;
    }

    private void stopTimer(View timer, String eventName, long eventTime) {
        currentTimerText.setTextColor(textColor);
        overtime = false;
        mTimer.cancel();
        ((TextView) timer.findViewById(R.id.text_timer_title)).setText(Utils.shortenString(getContext(), eventName));
        ((TextView) timer.findViewById(R.id.text_timer_time)).setText(Utils.formatMillisecond(eventTime));
        ((Globals) getActivity().getApplication()).currentActiveTimerIndex = -1;
        if (getResources().getDisplayMetrics().densityDpi == DisplayMetrics.DENSITY_HIGH) {
            ((ImageButton) timer.findViewById(R.id.button_timer_left))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_play_arrow_black_24dp));
            ((ImageButton) timer.findViewById(R.id.button_timer_right))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_edit_black_24dp));
        } else {
            ((ImageButton) timer.findViewById(R.id.button_timer_left))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_play_arrow_black_36dp));
            ((ImageButton) timer.findViewById(R.id.button_timer_right))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_edit_black_36dp));
        }

        // dismiss notification
        timerNotification.cancelNotification();
        if (notificationTimer != null) {
            notificationTimer.cancel();
            notificationTimer.purge();
        }

        // save data
        currentAction.endTime = System.currentTimeMillis();
        currentAction.save();
    }
}
