package edu.neu.madcourse.zhongxiruihao.pomodonut.utils;

import android.content.Context;
import android.util.DisplayMetrics;

import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import edu.neu.madcourse.zhongxiruihao.pomodonut.R;

/**
 * Created by huangruihao on 4/22/17.
 */

public class Utils {

    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int SECONDS_PER_MINUTE = 60;
    private static final int MINUTES_PER_HOUR = 60;

    private static final int LENGTH_OF_ELLIPSIS = 3;
    private static final String ELLIPSIS = "...";

    //  format: HH:mm:ss
    public static String formatMillisecond(long ms) {
        int seconds = (int) (ms / MILLISECONDS_PER_SECOND);
        long s = seconds % SECONDS_PER_MINUTE;
        long m = (seconds / SECONDS_PER_MINUTE) % MINUTES_PER_HOUR;
        long h = seconds / (SECONDS_PER_MINUTE * MINUTES_PER_HOUR);
        return String.format(Locale.US, "%02d:%02d:%02d", h, m, s);
    }

    //  format: HH:mm:ss
    public static long parseMillisecond(String formatTime){
        int hours = Integer.parseInt(formatTime.split(":")[0]);
        int minutes = Integer.parseInt(formatTime.split(":")[1]);
        int seconds = Integer.parseInt(formatTime.split(":")[2]);
        return MILLISECONDS_PER_SECOND * (hours * MINUTES_PER_HOUR * SECONDS_PER_MINUTE
                + minutes * SECONDS_PER_MINUTE + seconds);
    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static ArrayList<Integer> getColors() {
        ArrayList<Integer> colors = new ArrayList<>();

        colors.add(ColorTemplate.getHoloBlue());
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        return colors;
    }

    public static String shortenString(Context context, String s) {
        if (s.length() > context.getResources().getInteger(R.integer.string_max_length)
                + LENGTH_OF_ELLIPSIS) {
            return s.substring(0, context.getResources().getInteger(R.integer.string_max_length))
                    + ELLIPSIS;
        } else {
            return s;
        }
    }

    // get unix time of the start of the day
    public static long getStartUnix(int year, int month, int date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
                Locale.getDefault());
        String dateString = String.format(Locale.US, "%04d", year) + "-"
                + String.format(Locale.US, "%2d", month) + "-"
                + String.format(Locale.US, "%2d", date) + "T"
                + "00:00:00Z";
        long startUnix = 0;
        try {
            Date d = simpleDateFormat.parse(dateString);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            startUnix = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startUnix;
    }

    // get unix time of the end of the day
    public static long getEndUnix(int year, int month, int date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'",
                Locale.getDefault());
        String dateString = String.format(Locale.US, "%04d", year) + "-"
                + String.format(Locale.US, "%2d", month) + "-"
                + String.format(Locale.US, "%2d", date) + "T"
                + "23:59:59Z";
        long endUnix = 0;
        try {
            Date d = simpleDateFormat.parse(dateString);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            endUnix = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return endUnix;
    }
}
