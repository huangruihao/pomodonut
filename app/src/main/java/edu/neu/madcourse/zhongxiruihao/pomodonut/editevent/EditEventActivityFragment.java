package edu.neu.madcourse.zhongxiruihao.pomodonut.editevent;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.orm.SugarContext;

import java.util.List;
import java.util.zip.Inflater;

import edu.neu.madcourse.zhongxiruihao.pomodonut.R;
import edu.neu.madcourse.zhongxiruihao.pomodonut.countdowntimers.models.Event;
import edu.neu.madcourse.zhongxiruihao.pomodonut.globals.Globals;
import edu.neu.madcourse.zhongxiruihao.pomodonut.utils.Utils;

/**
 * A placeholder fragment containing a simple view.
 */
public class EditEventActivityFragment extends Fragment {

    private static final String EVENT_NAME_ARGS_KEY = "event name args key";
    private static final String EVENT_TIME_ARGS_KEY = "event duration args key";

    private static final int MILLISECONDS_PER_SECOND = 1000;
    private static final int MINUTES_PER_HOUR = 60;
    private static final int SECONDS_PER_MINUTE = 60;

    private String eventName;
    private long eventTime;

    private EditText editEventName;
    private NumberPicker hoursPicker, minutesPicker;

    public static EditEventActivityFragment newInstance(String eventName, long eventTime) {
        EditEventActivityFragment fragment = new EditEventActivityFragment();
        Bundle args = new Bundle();
        args.putString(EVENT_NAME_ARGS_KEY, eventName);
        args.putLong(EVENT_TIME_ARGS_KEY, eventTime);
        fragment.setArguments(args);
        return fragment;
    }

    public EditEventActivityFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventName = getArguments().getString(EVENT_NAME_ARGS_KEY);
            eventTime = getArguments().getLong(EVENT_TIME_ARGS_KEY);
        }
        setHasOptionsMenu(true);
        SugarContext.init(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_edit_event, container, false);

        editEventName = (EditText) root.findViewById(R.id.edit_event_name);
        hoursPicker = (NumberPicker) root.findViewById(R.id.picker_hours);
        minutesPicker = (NumberPicker) root.findViewById(R.id.picker_minutes);

        if (eventName != null) {
            editEventName.setText(eventName);
        }

        if (editEventName.getText().toString().equals("")) {
            root.findViewById(R.id.pickers_layout).setVisibility(View.GONE);
            root.findViewById(R.id.text_event_time).setVisibility(View.GONE);
            setHasOptionsMenu(false);
        } else {
            root.findViewById(R.id.pickers_layout).setVisibility(View.VISIBLE);
            root.findViewById(R.id.text_event_time).setVisibility(View.VISIBLE);
            setHasOptionsMenu(true);
        }

        editEventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    root.findViewById(R.id.pickers_layout).setVisibility(View.GONE);
                    root.findViewById(R.id.text_event_time).setVisibility(View.GONE);
                    setHasOptionsMenu(false);
                } else {
                    root.findViewById(R.id.pickers_layout).setVisibility(View.VISIBLE);
                    root.findViewById(R.id.text_event_time).setVisibility(View.VISIBLE);
                    setHasOptionsMenu(true);
                    if (hoursPicker.getValue() != 0 || minutesPicker.getValue() != 0) {
                        setHasOptionsMenu(true);
                    } else {
                        setHasOptionsMenu(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        hoursPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal != 0 && !editEventName.getText().toString().isEmpty()) {
                    setHasOptionsMenu(true);
                } else {
                    setHasOptionsMenu(false);
                }
            }
        });

        minutesPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal != 0 && !editEventName.getText().toString().isEmpty()) {
                    setHasOptionsMenu(true);
                } else {
                    setHasOptionsMenu(false);
                }
            }
        });

        if (eventTime != -1) {
            int seconds = (int) (eventTime / MILLISECONDS_PER_SECOND);
            int m = (seconds / SECONDS_PER_MINUTE) % MINUTES_PER_HOUR;
            int h = seconds / (SECONDS_PER_MINUTE * MINUTES_PER_HOUR);
            setPicker(hoursPicker, getResources().getInteger(R.integer.hours_picker_max_hour), false, h);
            setPicker(minutesPicker, MINUTES_PER_HOUR, true, m);
        } else {
            setPicker(hoursPicker, getResources().getInteger(R.integer.hours_picker_max_hour), false, 0);
            setPicker(minutesPicker, MINUTES_PER_HOUR, true, 0);
        }

        if (hoursPicker.getValue() != 0 || minutesPicker.getValue() != 0) {
            setHasOptionsMenu(true);
        } else {
            setHasOptionsMenu(false);
        }

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit_event, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                if (eventName == null) {  // new event
                    String newEventName = editEventName.getText().toString();
                    if (newEventName.isEmpty()) {
                        Toast.makeText(getActivity(), getString(R.string.toast_event_name_empty), Toast.LENGTH_LONG).show();
                    } else {
                        if (Event.find(Event.class, "name = ?", newEventName).isEmpty()) {
                            int hours = hoursPicker.getValue();
                            int minutes = minutesPicker.getValue();
                            long total = MILLISECONDS_PER_SECOND * (hours * MINUTES_PER_HOUR * SECONDS_PER_MINUTE
                                    + minutes * SECONDS_PER_MINUTE);
                            if (total == 0) {
                                Toast.makeText(getActivity(), getString(R.string.toast_zero_duration), Toast.LENGTH_LONG).show();
                            } else {
                                Event event = new Event(newEventName, total);
                                event.save();
                                ((Globals) getActivity().getApplication()).needRefreshTimers = true;
                                getActivity().onBackPressed();
                            }
                        } else {  // exist
                            Toast.makeText(getActivity(), getString(R.string.toast_event_exists), Toast.LENGTH_LONG).show();
                        }
                    }
                } else {  // edit event
                    List<Event> originalEvents = Event.find(Event.class, "name = ?", eventName);
                    if (originalEvents.size() == 1) {
                        Event originalEvent = originalEvents.get(0);
                        String newEventName = editEventName.getText().toString();
                        if (newEventName.isEmpty()) {
                            Toast.makeText(getActivity(), getString(R.string.toast_event_name_empty), Toast.LENGTH_LONG).show();
                        } else {
                            if (Event.find(Event.class, "name = ?", newEventName).isEmpty()
                                    || newEventName.equals(eventName)) {
                                int hours = hoursPicker.getValue();
                                int minutes = minutesPicker.getValue();
                                long total = MILLISECONDS_PER_SECOND * (hours * MINUTES_PER_HOUR * SECONDS_PER_MINUTE
                                        + minutes * SECONDS_PER_MINUTE);
                                if (total == 0) {
                                    Toast.makeText(getActivity(), getString(R.string.toast_zero_duration), Toast.LENGTH_LONG).show();
                                } else {
                                    originalEvent.name = newEventName;
                                    originalEvent.duration = total;
                                    originalEvent.save();
                                    getActivity().onBackPressed();
                                }
                            } else {  // exist
                                Toast.makeText(getActivity(), getString(R.string.toast_event_exists), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
                return true;
            default:
                break;
        }

        return false;
    }

    private void setPicker(NumberPicker picker, int maxValue, boolean allowWrapSelectorWheel, int value) {
        String[] hours = new String[maxValue + 1];
        for (int i = 0; i < maxValue + 1; ++i) {
            hours[i] = String.valueOf(i);
        }
        picker.setDisplayedValues(hours);
        picker.setWrapSelectorWheel(allowWrapSelectorWheel);
        picker.setMinValue(0);
        picker.setMaxValue(maxValue);
        picker.setValue(value);
    }
}
