package edu.neu.madcourse.zhongxiruihao.pomodonut.globals;

import android.app.Application;

/**
 * Created by huangruihao on 4/25/17.
 */

public class Globals extends Application {

    public boolean hasTimerCountingDown = false;

    // the index of the current active timer. Even if it is paused, it is still active.
    // As long as it is not stopped, it is active.
    public int currentActiveTimerIndex = -1;  // no timer is counting down

    public boolean needRefreshTimers = false;

}
