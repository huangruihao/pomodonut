package edu.neu.madcourse.zhongxiruihao.pomodonut.countdowntimers;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.orm.SugarContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.neu.madcourse.zhongxiruihao.pomodonut.R;
import edu.neu.madcourse.zhongxiruihao.pomodonut.countdowntimers.models.Event;
import edu.neu.madcourse.zhongxiruihao.pomodonut.dayview.DayViewActivity;
import edu.neu.madcourse.zhongxiruihao.pomodonut.donuts.DonutActivity;
import edu.neu.madcourse.zhongxiruihao.pomodonut.globals.Globals;
import edu.neu.madcourse.zhongxiruihao.pomodonut.sensor.AccelProcessService;
import edu.neu.madcourse.zhongxiruihao.pomodonut.sensor.RecordAccelService;

public class CountdownTimersActivity extends AppCompatActivity {

    private static final int MAX_PAGES = 4;
    public static final int TIMERS_PER_PAGE = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countdown_timers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SugarContext.init(getApplicationContext());

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        setCountdownTimers();

        startSensorServices();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_day_view:
                startActivity(new Intent(CountdownTimersActivity.this, DayViewActivity.class));
                return true;
            case R.id.action_donut:
                startActivity(new Intent(CountdownTimersActivity.this, DonutActivity.class));

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        if (((Globals) getApplication()).needRefreshTimers) {
            setCountdownTimers();
        }
    }



    private void startSensorServices(){
       if (!isMyServiceRunning(RecordAccelService.class)){
            startService(new Intent(getBaseContext(), RecordAccelService.class));
//            Toast.makeText(this, "RecordAccelService started", Toast.LENGTH_LONG).show();
        }

        if (!isMyServiceRunning(AccelProcessService.class)){
            startService(new Intent(getBaseContext(), AccelProcessService.class));
//            Toast.makeText(this, "AccelProcess started", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass){
        ActivityManager manager=(ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service: manager.getRunningServices(Integer.MAX_VALUE)){
            if (serviceClass.getName().equals(service.service.getClassName())){
                return true;
            }
        }
        return false;
    }

    public void stopSensorServices(View view){
        stopService(new Intent(getBaseContext(),RecordAccelService.class));
        stopService(new Intent(getBaseContext(),AccelProcessService.class));
    }

    private Event[] getEvents() {
        List<Event> eventList = Event.listAll(Event.class);
        Collections.sort(eventList, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                return o1.getFrequency() > o2.getFrequency() ? -1 : (o1.getFrequency() < o2.getFrequency() ) ? 1 : 0;
            }
        });
        Event[] events = new Event[eventList.size()];
        for (int i = 0; i < events.length; ++i) {
            events[i] = new Event(eventList.get(i).name, eventList.get(i).duration);
        }        return events;
    }

    private void setCountdownTimers() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        ArrayList<CountdownTimersFragment> fragments = new ArrayList<>();
        Event[] events = getEvents();  // events that are going to show on the main screen
        // TODO: handle scenario when events.length == 0
        int pages = (events.length + 1) / TIMERS_PER_PAGE
                + ((events.length + 1) % TIMERS_PER_PAGE == 0 ? 0 : 1);
        if (pages > MAX_PAGES) {
            pages = MAX_PAGES;
        }
        for (int i = 0; i < pages; ++i) {
            Event[] eventsCurrentPage;
            if (i == 0) {
                if (pages == 1) {  // no more than (TIMERS_PER_PAGE - 1) timers
                    eventsCurrentPage = Arrays.copyOfRange(events,
                            0, events.length + 1);
                } else {
                    eventsCurrentPage = Arrays.copyOfRange(events, 0, TIMERS_PER_PAGE);
                }
            } else {
                if (i < pages - 1) {  // page in the middle
                    eventsCurrentPage = Arrays.copyOfRange(events,
                            i * TIMERS_PER_PAGE - 1, (i + 1) * TIMERS_PER_PAGE - 1);
                } else {
                    eventsCurrentPage = Arrays.copyOfRange(events,
                            i * TIMERS_PER_PAGE - 1, events.length);
                }
            }
            fragments.add(CountdownTimersFragment.newInstance(eventsCurrentPage, i));
        }
        viewPager.setAdapter(new CountdownTimersPagerAdapter(getSupportFragmentManager(), fragments));
        ((Globals) getApplication()).needRefreshTimers = false;
    }
}
